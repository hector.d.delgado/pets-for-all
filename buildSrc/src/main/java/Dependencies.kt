object Dependencies {
    object AndroidX {
        object Activity {
            private const val VERSION = "1.7.0"
            
            // implementation
            val activityCompose by lazy { "androidx.activity:activity-compose:$VERSION" }
        }
        object Compose {
            object Material3 {
                private const val VERSION = "1.1.0-beta02"
                
                // implementation
                val core by lazy { "androidx.compose.material3:material3:$VERSION"}
            }
            
            object UI {
                private const val VERSION = "1.4.1"
                
                // implementation
                val core by lazy { "androidx.compose.ui:ui:$VERSION" }
                val toolingPreview by lazy { "androidx.compose.ui:ui-tooling-preview:$VERSION" }

                // androidTestImplementation
                val testJUnit by lazy { "androidx.compose.ui:ui-test-junit4:$VERSION" }
                
                // debugImplementation
                val tooling by lazy { "androidx.compose.ui:ui-tooling:$VERSION" }
                val testManifest by lazy { "androidx.compose.ui:ui-test-manifest:$VERSION" }
            }
        }
        object Core {
            private const val VERSION = "1.10.0"

            // implementation
            val coreKtx by lazy { "androidx.core:core-ktx:$VERSION" }
        }
        object Lifecycle {
            private const val VERSION = "2.6.1"

            // implementation
            val runtimeKtx by lazy { "androidx.lifecycle:lifecycle-runtime-ktx:$VERSION" }
        }

        object Test {
            object Ext {
                private const val VERSION = "1.1.5"
                
                // androidTestImplementation
                val jUnit by lazy { "androidx.test.ext:junit:$VERSION"}
            }
            object Espresso {
                private const val VERSION = "3.5.1"

                // androidTestImplementation
                val core by lazy { "androidx.test.espresso:espresso-core:$VERSION"}
            }
        }
    }
    
    object JUnit {
        private const val VERSION = "4.13.2"

        // testImplementation
        val core by lazy { "junit:junit:$VERSION" }
    }
}