plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.hectordelgado.petsforall"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.hectordelgado.petsforall"
        minSdk = 26
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.4"
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation(Dependencies.AndroidX.Activity.activityCompose)
    implementation(Dependencies.AndroidX.Compose.Material3.core)
    implementation(Dependencies.AndroidX.Compose.UI.core)
    implementation(Dependencies.AndroidX.Compose.UI.toolingPreview)
    implementation(Dependencies.AndroidX.Core.coreKtx)
    implementation(Dependencies.AndroidX.Lifecycle.runtimeKtx)

    testImplementation(Dependencies.JUnit.core)

    androidTestImplementation(Dependencies.AndroidX.Test.Ext.jUnit)
    androidTestImplementation(Dependencies.AndroidX.Test.Espresso.core)
    androidTestImplementation(Dependencies.AndroidX.Compose.UI.testJUnit)

    debugImplementation(Dependencies.AndroidX.Compose.UI.tooling)
    debugImplementation(Dependencies.AndroidX.Compose.UI.testManifest)
}